package notification;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.amazonaws.services.lambda.runtime.events.SQSEvent.SQSMessage;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class App implements RequestHandler<SQSEvent, Void> {

    @Override
    public Void handleRequest(SQSEvent event, Context context) {
        System.out.println("Environment => " + System.getenv("ENV"));
        StringBuffer buffer = new StringBuffer();
        for (SQSMessage msg : event.getRecords()) {
            buffer.append(msg.getBody());
        }
        System.out.println("Data =>");
        System.out.println(buffer.toString());

        Region region = Region.US_EAST_1;
        S3Client s3 = S3Client.builder().region(region).build();
        String bucket = "passenger-manifest-data-bucket";
        SimpleDateFormat etDf = new SimpleDateFormat("yyyyMMddHHmm'.txt'");
        TimeZone etTimeZone = TimeZone.getTimeZone("America/New_York");
        etDf.setTimeZone(etTimeZone);
        String fileName = etDf.format(new Date());
        System.out.println("File =>");
        String key = "report_" + fileName;
        System.out.println(key);
        s3.putObject(PutObjectRequest.builder().bucket(bucket).key(key)
                        .build(),
                RequestBody.fromString(buffer.toString()));
        return null;
    }
}
